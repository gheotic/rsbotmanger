import Vue from "vue";
import { store } from "../_store/index";
import Router from "vue-router";

import HomePage from "../views/Home.vue";
import LoginPage from "../views/Login.vue";
import RegisterPage from "../views/Register.vue";
import MyBots from "../views/MyBots.vue";

Vue.use(Router);

const router = new Router({
  routes: [
    { path: "/", meta: { guest: false }, component: HomePage },
    { path: "/home", meta: { guest: false }, component: HomePage },
    { path: "/login", meta: { guest: true }, component: LoginPage },
    { path: "/register", meta: { guest: true }, component: RegisterPage },
    { path: "/mybots", meta: { guest: false }, component: MyBots },

    { path: "*", redirect: "/" }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.meta.guest && store.state.account.status.loggedIn) next("/");
  if (!to.meta.guest && !store.state.account.status.loggedIn) next("/login");
  next();
});

export { router };
