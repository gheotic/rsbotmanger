import { botServices } from "../_services";

const state = {
  all: {}
};

const actions = {};

export const bots = {
  namespaced: true,
  state,
  actions
};
