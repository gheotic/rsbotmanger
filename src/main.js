import Vue from "vue";
import App from "./App.vue";
import { router } from "./_helpers/router";
import { store } from "./_store/index";
import VeeValidate from "vee-validate";

Vue.use(VeeValidate);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
